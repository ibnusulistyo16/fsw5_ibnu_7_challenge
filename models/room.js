'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static checkRoom = (id) => {
      return this.findByPk({
        username,
        password: encryptedPassword,
        role,
      })
    }

    static cekRoom = async (id) => {
      try {
        const room = await this.findOne({ where: { id }})
        if (!room) {
          return Promise.reject("Room not found!");
        }
        return Promise.resolve(room)
      } catch(err) {
        return Promise.reject(err.message)
      }
    }

  };
  Room.init({
    room: DataTypes.STRING,
    player1: DataTypes.INTEGER,
    player2: DataTypes.INTEGER,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Room',
  });
  return Room;
};