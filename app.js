const express = require('express');
const morgan = require('morgan');
// const passport = require('./lib/passport');

const app = express();

// Middleware
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended:false}));

// Menggunakan Middleware Passport
// app.use(passport.initialize());

// Routing
const router = require('./routes/index.routes');
app.use(router);


app.use((req,res, next)=>{
    const error = new ("Not Found!");
    error.status=404;
    next(error);
})

app.use((error, req, res, next)=>{
    res.status(error.status|| 500);
    res.json({
        error:{
            message:error.message,
        }
    })
})
module.exports = app;
