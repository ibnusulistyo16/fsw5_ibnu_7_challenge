const express = require('express');
const router = express.Router();

const authController = require('../controllers/auth.controller');
const homeController = require('../controllers/home.controller');
// const gameAPIController = require('../controllers/game.controller');
// Middleware
const {restrict} = require('../middlewares/restrict')

// Homepage
router.get('/api/v1/', restrict, homeController.index)

// Register
router.post('/api/v1/auth/register', authController.register);

// Login
router.post('/api/v1/auth/login', authController.login);

//create room
router.post('/api/v1/room/create', authController.createRoom);
//cek room
router.get('/api/v1/room',restrict, authController.checkRoom);
//cek room id
router.get('/api/v1/room/:id',restrict, authController.checkRoomId);
//enter room
router.post('/api/v1/room/:id', authController.enterRoom);

// fight

// Whoami page
router.get('/api/v1/auth/whoami', restrict, authController.whoami);

// Logout
router.post('/api/v1/auth/logout', restrict, authController.logout);

module.exports = router;
