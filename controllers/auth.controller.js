// const passport = require('passport');
let bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');
const jwt_decode = require('jwt-decode')
const {User,Room} = require('../models');
const room = require('../models/room');


const register = (req, res, next) => {
  User.register(req.body)
    .then((user) => {
      console.log(user);
      res.status(200).json(user);
    })
    .catch((error) => next(error.message))
};

const login =  (req, res, next) => {
  User.authenticate(req.body)
    .then(user => {
      dataUser = {
        id: user.id,
        username: user.username,
        token: user.generateToken()
      }
      res.status(200).json(dataUser)
    })
    .catch(error => {
      return next(error.message)
    })
}

const whoami = (req, res) => {
  console.log(req);
  res.status(200).json(req.user);
}

const checkRoom = async(req,res)=>{
  Room.findAll({attributes: ['id','room','player1','player2','status']})
  .then((room) => res.status(200).json({room }))
  .catch((error) => res.status(500).json({ error: error.name }));
}
const checkRoomId = async(req,res)=>{
  Room.findOne({where:{id:req.params.id}})
  .then((room) => res.status(200).json({room }))
  .catch((error) => res.status(500).json({ error: error.name }));
}

const createRoom = (req, res, next) => {
  const token = req.headers.authorization
  const decoded = jwt_decode(token)
  Room.create({
    room: req.body.room
})
    .then((room) => {
      console.log(room);
      res.status(200).json(room);
    })
    .catch((error) => next(error.message))
};

const enterRoom =  (req, res) => {
  const token = req.headers.authorization
  const decoded = jwt_decode(token)
  const status = {
    WAITING: 'waiting',
    FULL: 'full',
  };
  Room.cekRoom(req.params.id)
    .then(room => {
        if(room.status!== status.FULL){ // status waiting
          if(!room.player1){ // player 1 kosong
            if(room.player2){ //room player2 ada isi
              Room.update({ player1: decoded.id, status: status.FULL },{ where:{ id:req.params.id}})
              .then((r)=>{
                res.status(200).json({message: 'Player1 terisi'});
              })          
            }
            else{ //room player2 kosong  
              Room.update({ player1: decoded.id, status: status.WAITING },{where:{id:req.params.id}})
              .then((r)=>{
                res.status(200).json({message: 'Player1 terisi'});
              })    
            }
          }
          else if(!room.player2){ 
            if(room.player1){ //room player1 ada isi
              Room.update({ player2: decoded.id, status: status.FULL },{where:{id:req.params.id}}) 
              .then((r)=>{
                res.status(200).json({message: 'Player2 terisi'});
              })         
            }
            else{ //room player1 kosong    
              Room.update({ player2: decoded.id, status: status.WAITING },{where:{id:req.params.id}})
              .then((r)=>{
                res.status(200).json({message: 'Player2 terisi'});
              })      
            }
          } 
        } else{
          res.status(200).json({message: 'ROOM PENUH'});            
        }
    }).catch((error) => next(error.message))
}

const logout = (req, res) => {
  // req.logout();
  // res.redirect('/login');
  res.status(200);
}

module.exports = {
  register,
  login,
  whoami,
  checkRoom,
  checkRoomId,
  createRoom,
  enterRoom,
  logout
};
